#!/bin/bash

# Neutrinet SD Card Installer
# Copyright (C) 2019 Ilja Baert <neutrinet@spectraltheorem.be>
# Parts of this script are based on and/or copied from neutrinet.sh and 
# install-sd.sh from the Neutrinet and labriqueinternet projects.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


#=================================================
# FUNCTIONS
#=================================================

function show_welcome() {
  clear
  cat <<EOF
********************************************************************************
You are about to configure a Neutrinet Internet Cube. If you do
not know what this is, please visit https://neutrinet.be/en/brique
for more information.

Before you start this script, we assume you have the following:
* Either an A20-OLinuXino-LIME or an A20-OLinuXino-LIME2 board
* 5V DC power supply for the board
* Minimum 16G micro sd card
* Wifi USB antenna
* Preferably a case
* Optionally a neutrinet VPN account and certificates

You also need an internet connection on this machine while running the script and
an ethernet connection for the cube. Preferably the cube will be on the same local
network as this machine. You also need to be able to connect the micro sd card to 
this machine.

/!\\ Be aware that as soon as the vpn goes live the root user can log in over
    the vpn with the chosen password! Choosing a dictionary word or 12345678 is
    not the best thing to do here, instead have a look at
    https://ssd.eff.org/en/module/creating-strong-passwords for advice on
    creating a strong password.

Press ENTER to continue or CTRL-C to abort
EOF
  read -s
}

function get_hypercube_file() {

  echo
  echo "What domain name do you want to use? (will be used to host your email and services)"
  echo "You can either use your own domain, or freely use a subdomain under one of the following domains:"
  echo "  .nohost.me"
  echo "  .noho.st"
  echo "  .ynh.fr"
  echo "i.e.: example.nohost.me"
  read domain
  domain_available=false
  while ( ! $domain_available )
  do
    if grep -e '.noho.st$' -e '.nohost.me$' -e '.ynh.fr$' <<< "$domain"
    then # ynh dyndns
      echo "You've chosen one of the free subdomains. Checking availability..."
      
      if curl --fail --silent "https://dyndns.yunohost.org/test/$domain"
      then
        domain_available=true
      else
        echo
        echo "$domain is not available. Please choose another domain"
        echo "You can either use your own custom domain, or freely use a subdomain under one of the following doamains:"
        echo "  .nohost.me"
        echo "  .noho.st"
        echo "  .ynh.fr"
        echo "i.e.: example.nohost.me"
        read domain
      fi
    else # own domain
      domain_available=true
    fi
  done

  echo
  echo "Username (used to connect to the user interface and access your apps, must be composed of lowercase letters and numbers only)"
  echo "i.e.: jonsnow"
  read username
  while [[ -z $username ]] # TODO: test with regex
  do
    echo "Please provide a username (used to connect to the user interface and access your apps, must be composed of lowercase letters and numbers only)"
    echo "i.e.: jonsnow"
    read username
  done

  echo
  echo "Firstname (mandatory, used as your firstname when you send emails)"
  echo "i.e.: Jon"
  read firstname
  while [[ -z $firstname ]] # TODO: check if there are constraints (min length, etc.)
  do
    echo "Please provide a firstname (mandatory, used as your firstname when you send emails)"
    echo "i.e.: Jon"
    read firstname
  done

  echo
  echo "Lastname (mandatory, used as your lastname when you send emails)"
  echo "i.e. Snow"
  read lastname
  while [[ -z $lastname ]] # TODO: check if there are constraints (min length, etc.)
  do
    echo "Please provide a lastname (mandatory, used as your lastname when you send emails)"
    echo "i.e. Snow"
    read lastname
  done

  echo
  echo "User password (Use a strong password!)"
  echo "See https://ssd.eff.org/en/module/creating-strong-passwords for advice"
  read -s user_pwd # TODO: check if there are constranits (emptiness, min length, etc.), maybe ask twice the password

  echo
  echo "Admin password (Use a strong password! This will access the admin screen of your internetcube)"
  echo "See https://ssd.eff.org/en/module/creating-strong-passwords for advice"
  read -s admin_pwd # TODO: check if there are constraints (emptiness, min length, etc.), maybe ask twice the password

  # TODO: The following should be automated
  # See https://github.com/Neutrinet/scripts/tree/master/vpn for inspiration
  # We could download the configuration package from the web interface?
  echo
  echo "You will now need to enter your neutrinet vpn certificates and credentials"
  echo "If you want to reuse certificates from a previous install, you can find everything on that cube as user.crt, user.key, ca-server.crt and credentials in /etc/openvpn/keys"
  echo "If you do not have an account yet, please see https://wiki.neutrinet.be/vpn/order"
  echo
  echo "VPN client certificate (paste all the content of client.crt below and end with a blank line): "
  vpn_client_crt=$(sed '/^$/q' | sed '/^$/d') # TODO: figure out this black magic
  echo
  echo "VPN client key (paste all the content of client.key below and end with a blank line): "
  vpn_client_key=$(sed '/^$/q' | sed '/^$/d')
  echo
  echo "CA server certificate (paste all the content of ca.crt below and end with a blank line): "
  vpn_ca_crt=$(sed '/^$/q' | sed '/^$/d')
  echo
  echo "VPN username (first line of the 'auth' file): "
  read vpn_username
  echo
  echo "VPN password (second line of the 'auth' file): "
  read -s vpn_pwd
  echo
  echo "ALRIGHT! That was it for the certificates ^^ Let's continue with the rest, shall we"
  echo

  echo
  echo "WiFi hotspot name"
  echo "i.e.: MyWunderbarNeutralNetwork"
  read wifi_ssid # TODO: check if there are constraints + emptiness
  echo
  echo "WiFi hotspot password"
  echo "i.e.: MyWunderbarNeutralNetwork"
  read -s wifi_pwd # TODO: check if there are constraints + emptiness
  echo

  create_hypercubefile
}

function create_hypercubefile() {
  echo
  echo "Creating hypercubefile..."

  # Make template file
  hypercubefile="install.hypercube"

  vpn_client_crt="${vpn_client_crt//$'\n'/\|}"
  vpn_client_key="${vpn_client_key//$'\n'/\|}"
  vpn_ca_crt="${vpn_ca_crt//$'\n'/\|}"

  cat > $hypercubefile << EOF
{
  "vpnclient": {
    "server_name": "vpn.neutrinet.be",
    "server_port": "1195",
    "server_proto": "udp",
    "ip6_net": "",
    "ip4_addr": "",
    "crt_server_ca": "$vpn_ca_crt",
    "crt_client": "$vpn_client_crt",
    "crt_client_key": "$vpn_client_key",
    "crt_client_ta": "",
    "login_user": "$vpn_username",
    "login_passphrase": "$vpn_pwd",
    "dns0": "89.234.141.66",
    "dns1": "2001:913::8",
    "openvpn_rm": [
      ""
    ],
    "openvpn_add": [
      "cipher AES-256-CBC",
      "tls-version-min 1.2",
      "auth SHA256",
      "topology subnet"
    ]
  },
  "hotspot": {
    "wifi_ssid": "$wifi_ssid",
    "wifi_passphrase": "$wifi_pwd",
    "ip6_net": "",
    "ip6_dns0": "2001:913::8",
    "ip6_dns1": "2001:910:800::40",
    "ip4_dns0": "80.67.188.188",
    "ip4_dns1": "80.67.169.12",
    "ip4_nat_prefix": "10.0.242",
    "firmware_nonfree": "no"
  },
  "yunohost": {
    "domain": "$domain",
    "password": "$admin_pwd",
    "user": "$username",
    "user_firstname": "$firstname",
    "user_lastname": "$lastname",
    "user_password": "$user_pwd"
  },
  "unix": {
    "root_password": "$admin_pwd",
    "lang": "en"
  }
}
EOF
  echo "install.hypercube created"
}

function get_image() {
  echo
  echo "What hardware are you installing on? lime/lime2"
  read board
  while [[ $board != "lime" && "$board" != "lime2" ]]
  do
    echo "This script doesn't support this. Please choose lime/lime2"
    read board
  done

  download_image
}

function download_image() {
  INTERNETCUBE_PREFIX="internetcube"
  YUNOHOST_PREFIX="yunohost"
  
  echo
  echo "Finding image..."

  image=$(ls ../$CUBE_RESOURCES_LOCATION/${INTERNETCUBE_PREFIX}-${DEBIAN_CODENAME}-*-$board-*.img 2> /dev/null | head -n 1)
  if [[ -z $image ]]
  then
    image_zip=$(curl --show-error --fail --silent $YNH_IMG_LOCATION | grep "${YUNOHOST_PREFIX}-${DEBIAN_CODENAME}-[\d\.]+-$board-stable.img.zip" -Po | sort -V | tail -n 1)
    image=${image_zip%.zip}
    
    echo "Downloading $board image"
    curl --show-error --fail --progress-bar --output "$image_zip" "$YNH_IMG_LOCATION/$image_zip"

    echo
    echo "Checking image integrity..."
    image_sha256sum=$(curl --show-error --fail --silent $YNH_IMG_LOCATION/$image_zip.sha256sum || true)
    if [[ -z $image_sha256sum ]] || ! sha256sum -c <<< $image_sha256sum
    then
      >&2 echo "WARNING: Couldn't verify integrity of $image!!!"
      >&2 echo "Do you really want to continue with the installation? (yes/no)"
      read continue_install
      
      if [[ $continue_install != "yes" && $continue_install != "y" ]]
      then
        exit 1
      fi
    fi
    
    echo
    echo "Unzipping..."
    unzip "$image_zip"
    rm "$image_zip"
    
    image=$(ls -t ${YUNOHOST_PREFIX}-${DEBIAN_CODENAME}-*-$board-stable.img | head -n1)

    echo
    echo "Preparing as internetcube image..."
    check_sudo
    
    if [[ ! -f $CUBE_BUILD_SCRIPT_NAME ]]
    then
      git clone ${CUBE_BUILD_SCRIPT_LOCATION} ${CUBE_BUILD_SCRIPT_DIR}
      git -C ${CUBE_BUILD_SCRIPT_DIR} checkout ${CUBE_BUILD_SCRIPT_REVISION}
    fi
    
    echo
    echo "Checking internetcube build script integrity..."
    if ! sha256sum -c <<< "$CUBE_BUILD_SCRIPT_SHA256SUM $CUBE_BUILD_SCRIPT_NAME"
    then
      >&2 echo "WARNING: Couldn't check integrity of internetcube build script!!!"
      >&2 echo "Do you really want to continue with the installation? (yes/no)"
      read continue_install
      
      if [[ $continue_install != "yes" && $continue_install != "y" ]]
      then
        exit 1
      fi
    fi
    
    echo
    echo "Building internetcube image..."
    sudo bash $CUBE_BUILD_SCRIPT_NAME "$image"
    
    rm "$image"
    image=${image/${YUNOHOST_PREFIX}/${INTERNETCUBE_PREFIX}}
    
  fi
  echo "We've got it!"
}

function flash_sd_card() {
  echo
  echo "Let's flash the SD-card!"
  if [[ ! -f $INSTALL_SD_SCRIPT_NAME ]]
  then
    download_install_sd_script
  fi
  
  "./$INSTALL_SD_SCRIPT_NAME" -y "$hypercubefile" -f "$image"
}

function prepare_cube() {
  echo
  echo "Alright! Time to prepare the cube!"
  echo "Make sure the Cube is not connected to a power source (e.g: the power cable should be unplugged)"
  echo "1. Assemble the board and case"
  echo "2. Insert the WiFi antenna into the USB port of the Cube's board"
  echo "3. Insert the SD card into your Cube"
  echo "4. Connect your Cube to an ethernet cable that is connected to the internet"
  echo "5. Connect the power cable to the Cube to start it"
  echo "It may taken a couple of minutes before the cube has started up an is connected to the internet"
  echo "Once the cube has started up, it will setup everything based on the answers that where given"
  echo "Complete setup can take up to an hour or even more. Please be patient. It should not take two hours"
  echo "By the time the setup is complete, you will be able to connect through the vpn"
  echo
  echo "Press ENTER when the cube is connected"
  read -s
}

function search_cube() {
  if [[ ! -f $INSTALL_SD_SCRIPT_NAME ]] # TODO: check if it's needed
  then
    download_install_sd_script
  fi
  
  echo
  echo "Once your cube is started up, we can search for it on the local network so you can follow its progress"
  echo "Note that it can take a couple of minutes before the cube is discoverable"
  echo "Once found, you can follow the progress from the HyperCube Debug url which is of the form http://<local-ip>:2486/install.html"
  echo "Note that even after finding it it can take a minute or so before the HyperCube Debug url works"
  echo "You can also easily add the ip to your hosts file"
  echo "Do you want to search for your cube on the local network? (yes/no)" # TODO: add default to yes in a pretty way
  read search_cubes
  if [[ $search_cubes != "no" ]]
  then
    echo "Searching for cubes"
    continue_searching="yes"
    while [[ $continue_searching != "no" ]]
    do
      "./$INSTALL_SD_SCRIPT_NAME" -l || true
      echo "If we found it you should be able to follow the progress at the HyperCube Debug url"
      echo "Do you want to continue searching? (yes/no)" # TODO: add default to yes
      read continue_searching
    done
    echo
    echo "Ok! Note that the 'HyperCube Debug' url will be accessible up to four hours after completion"
  fi
}

function show_goodbye(){
  echo
  echo "Alright, that's all we could do for now. We hope you'll be happy with your cube!"
  echo "If you have problems, don't hesitate to reach out to us at"
  echo "* Our wiki: https://wiki.neutrinet.be/cube/problems"
  echo "* Our Mattermost: https://chat.neutrinet.be"
  echo "* On freenode: #neutrinet"
  echo "* Via mail: contact@neutrinet.be"
  echo
  echo "And if you want to contribute to this awesome project, don't hesitate to contact us as well ;)"
}

function check_sudo() {
  if ! command -v sudo > /dev/null
  then
    echo "sudo command is required"
    exit 1
  fi

  echo "This script needs a sudo access"

  if ! sudo echo > /dev/null
  then
    echo "sudo password is required"
    exit 1
  fi
}

function exit_script() {
  echo
  if $DEBUG
  then
    echo "Debug mode: press ENTER to clean up and exit"
    read -s
  fi
  
  echo "Cleaning up..."
  cd "../"
  rm -rf "$TMP_DIR"
}

function prepare() {
  echo "Setting up the $CUBE_RESOURCES_LOCATION folder"
  mkdir -p "$CUBE_RESOURCES_LOCATION"
  cd $CUBE_RESOURCES_LOCATION
  
  echo "Downloading and preparing the lime image"
  board=lime
  download_image
  echo "Downloading and preparing the lime2 image"
  board=lime2
  download_image
  
  rm -rf "$CUBE_BUILD_SCRIPT_DIR"
  
  download_install_sd_script
}

function show_help() {
  echo -e "\n       \e[7m\e[1m Neutrinet Cube installer \e[0m\n"
  echo -e "\e[1mOPTIONS\e[0m"
  echo -e "  \e[1m-p\e[0m" >&2
  echo -e "     Downloads and prepares all general images and files needed for installing the cube" >&2
  echo -e "     \e[2mNext time this script is ran from the same folder as it is ran now, these files and scrips will be used\e[0m" >&2
  echo -e "  \e[1m-h\e[0m" >&2
  echo -e "     Show this help" >&2
}

function download_install_sd_script() {
  echo
  echo "Downloading $INSTALL_SD_SCRIPT_NAME script..."
  curl --show-error --fail --silent --output "$INSTALL_SD_SCRIPT_NAME" "$INSTALL_SD_SCRIPT_LOCATION/$INSTALL_SD_SCRIPT_NAME"
  
  echo
  echo "Checking integrity of $INSTALL_SD_SCRIPT_NAME..."
  install_sd_md5sum=$(curl --show-error --fail --silent "$INSTALL_SD_SCRIPT_LOCATION/MD5SUMS" | grep $INSTALL_SD_SCRIPT_NAME || true)
  if [[ -z $install_sd_md5sum ]] || ! md5sum -c <<< $install_sd_md5sum
  then
    >&2 echo "WARNING: Cannot verify the integrity of $INSTALL_SD_SCRIPT_NAME!!!"
    >&2 echo "Do you really want to continue with the installation? (yes/no)"
    read continue_install
    
    if [[ $continue_install != "yes" ]] || [[ $continue_install != "y" ]]
    then
      exit 1
    fi
  fi
  
  chmod u+x "$INSTALL_SD_SCRIPT_NAME"
}

#=================================================
# GLOBAL VARIABLES
#=================================================
YNH_IMG_LOCATION="https://build.yunohost.org"
CUBE_BUILD_SCRIPT_LOCATION="https://github.com/labriqueinternet/build.labriqueinter.net.git"
CUBE_BUILD_SCRIPT_REVISION="9dad8a5a5e6c3127b756383f94027e7b595a89a1"
CUBE_BUILD_SCRIPT_DIR="yunocube"
CUBE_BUILD_SCRIPT_NAME="${CUBE_BUILD_SCRIPT_DIR}/yunocube.sh"
CUBE_BUILD_SCRIPT_SHA256SUM="a942ec46c7c2c6ad8e04219519a838f784465fe5fa65a1ccc1f2438ae08413be"
INSTALL_SD_SCRIPT_LOCATION="https://repo.labriqueinter.net"
INSTALL_SD_SCRIPT_NAME="install-sd.sh"
CUBE_RESOURCES_LOCATION="cube_resources"
DEBIAN_CODENAME="stretch"

#=================================================
# GET OPTIONS
#=================================================

DEBUG=false
while getopts "dhp" opt; do
  case $opt in
    d) DEBUG=true;;
    p) prepare; exit 0 ;;
    h) show_help; exit 0 ;;
    \?) show_help; exit 0 ;;
  esac
done

#=================================================
# SCRIPT
#=================================================

TMP_DIR=$(mktemp -dp . .neutrinetcube_tmpXXXXXX)
# Exit if any of the following command fails
set -e
cd $TMP_DIR
trap exit_script EXIT

show_welcome
get_hypercube_file
get_image
flash_sd_card
prepare_cube
search_cube
show_goodbye

